package services;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Permissions;
import domain.User;
import domain.UserManager;

@WebServlet("/permissions")
public class SetPermissions extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public SetPermissions() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		User user = new User();

		if(new UserManager().findUser(request.getParameter("username"))) {
			if(new UserManager().findPermission(request.getParameter("username")).equals(Permissions.USER))
			{
				new UserManager().SetPermission(request.getParameter("username"), Permissions.PREMIUM);
				response.getWriter().print("Nadano PREMIUM");
			}
			else if(new UserManager().findPermission(request.getParameter("username")).equals(Permissions.PREMIUM))
			{
				new UserManager().SetPermission(request.getParameter("username"), Permissions.USER);
				response.getWriter().print("Nadano USER");
			}
		}
		
		System.out.println(new UserManager().findPermission(request.getParameter("username")));
		
	}

}
