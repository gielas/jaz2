package domain;

public enum Permissions {
	
	GUEST, USER, PREMIUM, ADMIN
	
}
