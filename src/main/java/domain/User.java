package domain;

public class User{
	
	private String username;
	private String password;
	private String email;
	private Permissions permission;

	public String getUsername() {
		return username;
	}
	public User setUsername(String username) {
		this.username = username;
		return this;
	}
	public String getPassword() {
		return password;
	}
	public User setPassword(String password) {
		this.password = password;
		return this;
	}
	public String getEmail() {
		return email;
	}
	public User setEmail(String email) {
		this.email = email;
		return this;
	}
	public Permissions getPermission() {
		return permission;
	}
	public User setPermission(Permissions permission) {
		this.permission = permission;
		return this;
	}
	
}
