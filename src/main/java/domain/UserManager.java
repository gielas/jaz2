package domain;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

public class UserManager{
	
	private static List<User> db = new ArrayList<User>();

	
	public void add(HttpServletRequest request){
		
		User user = new User();

		user.setUsername(request.getParameter("username"))
			.setPassword(request.getParameter("password"))
			.setEmail(request.getParameter("email"))
			.setPermission(Permissions.USER);
		
		db.add(user);
	}
	
	public boolean findUser(String username){
		
		for(User user: db)
		{
			if (user.getUsername().equals(username))
			return true;
		}
		return false;
		
	}
	
public boolean findPassword(String password){
		
		for(User user: db)
		{
			if (user.getPassword().equals(password))
			return true;
		}
		return false;
		
	}

	public String getAllUsers(){

		String users = "";
		
		for(User user: db)
		{
			users+="Username: "+user.getUsername()+" E-Mail: "+user.getEmail()+" \n";
		}
		
		return users;
		
	}

	public Permissions findPermission(String username){
		
		for(User user: db)
		{
			if (user.getUsername().equals(username))
			return user.getPermission();
		}
		return null;
	}
		
	public Permissions SetPermission(String username, Permissions permission){
			
			for(User user: db)
			{
				if (user.getUsername().equals(username))
				user.setPermission(permission);
			}
			return null;
		
	}

}
