package domain;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

public class DatabaseConnector {
	
	private Connection connection;
	
	private String url = "jdbc:hsqldb:hsql://localhost/workdb";

	private String createTablePerson = "CREATE TABLE User(id bigint GENERATED BY DEFAULT AS IDENTITY, username varchar(20), password varchar(20), email varchar(20), permission varchar(20))";

	private PreparedStatement addUserStmt;
	private PreparedStatement getAllUsersStmt;

	private Statement statement;

	public void UserManager() {
		try {
			connection = DriverManager.getConnection(url);
			statement = connection.createStatement();

			ResultSet rs = connection.getMetaData().getTables(null, null, null,
					null);
			boolean tableExists = false;
			while (rs.next()) {
				if ("User".equalsIgnoreCase(rs.getString("TABLE_NAME"))) {
					tableExists = true;
					break;
				}
			}

			if (!tableExists)
				statement.executeUpdate(createTablePerson);

			addUserStmt = connection
					.prepareStatement("INSERT INTO User (username, password, email, permission) VALUES (?, ?, ?, ?)");
			getAllUsersStmt = connection
					.prepareStatement("SELECT username, email FROM User");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	Connection getConnection() {
		return connection;
	}

	public int add(HttpServletRequest request) {
		int count = 0;
		
		try {
			addUserStmt.setString(1, request.getParameter("username"));
			addUserStmt.setString(2, request.getParameter("password"));
			addUserStmt.setString(3, request.getParameter("email"));
			addUserStmt.setString(4, "USER");

			count = addUserStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return count;
	}

	public List<User> getAllUsers() {
		List<User> users = new ArrayList<User>();

		try {
			ResultSet rs = getAllUsersStmt.executeQuery();

			while (rs.next()) {
				User u = new User();
				u.setUsername(rs.getString("username"));
				u.setEmail(rs.getString("email"));
				users.add(u);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return users;
	}
	
}
