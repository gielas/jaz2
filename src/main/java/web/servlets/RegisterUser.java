package web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.UserManager;

@WebServlet("/registration")
public class RegisterUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public RegisterUser() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if(request.getParameter("password").equals(request.getParameter("passwordConfirm"))){
			new UserManager().add(request);
			response.sendRedirect("registred.jsp");
		}	
		else {
			request.setAttribute("status", "Passwords are not the same");
			response.sendRedirect("registration.jsp");
		}
	}
}
