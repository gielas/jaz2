package web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.UserManager;

@WebServlet("/login")
public class LoginUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public LoginUser() {
        super();
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		if(request.getParameter("register")!=null) {
			response.sendRedirect("registration.jsp");
		}
		else {
			
			if(request.getParameter("username").equals("") || request.getParameter("password").equals("")){
				request.setAttribute("status", "Both fields are required");
				response.sendRedirect("login.jsp");
			}
			else {
				if(new UserManager().findUser(request.getParameter("username"))) {
					if(new UserManager().findUser(request.getParameter("password"))){
						request.setAttribute("access", new UserManager().findPermission(request.getParameter("username")));
						response.sendRedirect("logged.jsp");
					}
				}
				else {
					request.setAttribute("status", "User or password is not correct");
					response.sendRedirect("login.jsp");
				}
			}
		}
	}
}
