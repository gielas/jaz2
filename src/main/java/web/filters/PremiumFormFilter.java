package web.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

//@WebFilter({"premium.jsp", "/premium"})
public class PremiumFormFilter implements Filter {

    public PremiumFormFilter() {
    }


	public void destroy() {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpSession session = httpRequest.getSession();
		String userLogged = (String) session.getAttribute("access");
		
		if(userLogged.equals("admin")) {
			chain.doFilter(request, response);
		}
		else {
			response.getWriter().println("Access denied");
		}
		
	}

	public void init(FilterConfig fConfig) throws ServletException {
	}

}